import { createStore, combineReducers, applyMiddleware } from "redux";
import createSagaMiddleware from 'redux-saga';
import { dataWatcher } from "../saga/userSaga";
import logoReducer from "../reducers/logo.reducer";
import productReducer from '../reducers/product.reducer';
import userReducer from "../reducers/profile.reducer";

const sagaMiddleware = createSagaMiddleware();

export const rootReducer = combineReducers({
    logo: logoReducer,
    products: productReducer,
    user: userReducer
});

export const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(dataWatcher);