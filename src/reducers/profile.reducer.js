import { user as defaultState } from "../defaultStates/user";
import { EDIT_USER, REQUEST_USER, REQUEST_USER_SUCCESS, REQUEST_USER_FAIL } from '../constants/profile.constants';

export default (state = defaultState, action) => {
    switch (action.type) {
        case EDIT_USER:
            let newState = {
                name: action.data.name,
                surname: action.data.surname,
                card: action.data.card
            };

            return newState;
        case REQUEST_USER:
            return {
                url: [],
                loading: true,
                error: false,
            };
        case REQUEST_USER_SUCCESS:
            return {
                url: action.url,
                loading: false,
                error: false,
            };
        case REQUEST_USER_FAIL:
            return {
                url: [],
                loading: false,
                error: true,
            };
        default:
            return state;
    }
};