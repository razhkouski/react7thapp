import { products as defaultState } from "../defaultStates/products";

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [...state, action.data];
        case 'DELETE_PRODUCT':
            return state.filter(el => el.id !== action.id)
        case 'EDIT_PRODUCT':
            let newState = [];
            state.forEach(el => el.id === action.data.id ? newState.push(action.data) : newState.push(el));
            return newState;
        case 'FIND_PRODUCT':
            if (action.data.length >= 3) {
                return state.filter(el => {
                    let re = new RegExp(`.?${action.data.toLowerCase()}+.?`);
                    return re.test(el.title.toLowerCase());
                })
            } else {
                return state;
            }
        case 'RESET_PRODUCTS':
            return defaultState;
        default:
            return state;
    }
};
