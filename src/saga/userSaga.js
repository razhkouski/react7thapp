import { put, call, takeEvery } from 'redux-saga/effects';
import { requestUser, requestUserSuccess, requestUserError } from '../actionCreators/profile.action';
import { FETCH_USER } from '../constants/profile.constants';


function* dataWorker() {
    try {
        yield put(requestUser());
        const data = yield call(() => fetch("https://jsonplaceholder.typicode.com/users/1").then(res => res.json()));
        yield put(requestUserSuccess(data));
    } catch (error) {
        yield put(requestUserError());
    }
}

export function* dataWatcher() {
    yield takeEvery(FETCH_USER, dataWorker)
}