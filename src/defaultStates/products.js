export const products = [
    {
        id: 1,
        title: 'Смартфон Apple iPhone 11 64GB (черный)',
        img: `https://content2.onliner.by/catalog/device/header/e2189f90f9088975c553ec33431fc186.jpeg`,
        description: `Apple iOS, экран 6.1" IPS (828x1792), Apple A13 Bionic, ОЗУ 4 ГБ, флэш-память 64 ГБ, камера 12 Мп, аккумулятор 3046 мАч, 1 SIM`,
        price: `1670.00`
    },
    {
        id: 2,
        title: `Смартфон Apple iPhone 12 Pro 128GB (графитовый)`,
        img: `https://content2.onliner.by/catalog/device/header/a5e2764b76188d4ffe32fefeb0a6b9be.jpeg`,
        description: `Apple iOS, экран 6.1" OLED (1170x2532), Apple A14 Bionic, ОЗУ 6 ГБ, флэш-память 128 ГБ, камера 12 Мп, аккумулятор 2775 мАч, 1 SIM`,
        price: `2829.00` 
    },
    {
        id: 3,
        title: `Смартфон Apple iPhone SE 64GB (черный)`,
        img: `https://content2.onliner.by/catalog/device/header/5967975d9c930761f16487f8b690eb9a.jpeg`,
        description: `Apple iOS, экран 4.7" IPS (750x1334), Apple A13 Bionic, ОЗУ 3 ГБ, флэш-память 64 ГБ, камера 12 Мп, аккумулятор 1820 мАч, 1 SIM`,
        price: `1187.00`
    }
];