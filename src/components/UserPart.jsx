import React, { useState } from 'react';
import { connect } from 'react-redux';
import { findProduct, resetProducts } from '../actionCreators/product.action';

function UserPart(props) {
    const [inputValue, setInputValue] = useState('');

    const handleSearch = () => {
        props.findProduct(inputValue);
        
        setInputValue('');
    };

    const handleReset = () => {
        props.resetProducts();
    }

    return (
        <div>
            <h1>Catalog</h1>
            <input value={inputValue} onChange={(e) => setInputValue(e.target.value)}></input>
            <button onClick={() => handleSearch()}>Find</button>
            <button onClick={() => handleReset()}>Reset</button>
            {
                props.products.map(item => (
                    <div key={item.id}>
                        <p>{item.title}</p>
                        <img src={item.img} alt="" />
                        <p>{item.description}</p>
                        <p>{item.price} BYN</p>
                    </div>
                ))
            }
        </div>
    )
}

const mapStateToProps = (state) => ({
    products: state.products
});

const mapDispatchToProps = (dispatch) => ({
    findProduct: (data) => dispatch(findProduct(data)),
    resetProducts: () => dispatch(resetProducts())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserPart);