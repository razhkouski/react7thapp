import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { editUser, fetchUser } from '../actionCreators/profile.action';

function Profile(props) {
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [username, setUsername] = useState();

    const handleEdit = () => {
        let user = {
            name: name ? name : props.user.url.name,
            email: email ? email : props.user.email,
            surname: email ? email : props.user.surname
        };

        props.editUser(user);

        setName('');
        setEmail('');
        setUsername('');
    }

    const handleList = () => {
        props.fetchUser()
    }

    return (
        <>
            <h1>User information</h1>
            <div key={props.user.id}>
                <p>Name: {props.user.url.name}</p>
                <p>Email: {props.user.url.email}</p>
                <p>Username: {props.user.url.username}</p>
            </div>
            Name: <input value={name} onChange={(e) => setName(e.target.value)}></input>
            Email: <input value={email} onChange={(e) => setEmail(e.target.value)}></input>
            Username: <input value={username} onChange={(e) => setUsername(e.target.value)}></input>
            <button onClick={() => handleEdit(props.user)}>Edit</button>
            <button onClick={() => handleList()}>Show userdata</button>
            {props.user.loading ? <p>Loading...</p> : 
                props.user.error ? <p>Error, try again</p> : 
                console.log('Success.')}
        </>
    )
};

const mapStateToProps = (state) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => ({
    editUser: (data) => dispatch(editUser(data)),
    fetchUser: () => dispatch(fetchUser())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);